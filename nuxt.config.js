const shrinkRay = require('shrink-ray-current');

export default {

  mode: 'universal',

  modern: (process.env.NODE_ENV == 'production') ? true : false,

  dev: (process.env.NODE_ENV !== 'production'),

  env: {
      baseUrl: (process.env.NODE_ENV == 'production') ? 'https://www.mauvegroup.co.uk' : 'http://localhost:3000'
  },

  /*
  ** Headers of the page
  */
  head: {
    title: 'Mauve Group',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0' },
      { hid: 'description', name: 'description', content: "Leading provider of Global Business Solutions and Consultancy Services, supporting organisations in more than 150 countries since 1996" }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' }
    ],
    script: [
      { type: 'text/javascript', src: "https://www.google-analytics.com/analytics.js", async: true},
      { type: 'text/javascript', src: "https://code.jquery.com/jquery-3.4.1.min.js"},
  
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#bcbade' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/fonts/fonts.css',
    '@/assets/scss/style.scss',
    'swiper/dist/css/swiper.css',
  ],

  /* Layout Transitions */
  layoutTransition: {
    name: "layout",
    mode: ""
  },
  
  /* Page Transitions */
  pageTransition: {
    name: "default",
    mode: ""
  },

  render: {

    compressor: shrinkRay(),

    bundleRenderer: {

        // shouldPrefetch: (file, type) => {
        //     return ['script', 'style'].includes(type)
        // }

    },

    http2: {
        push: true
    },

    static: {
        maxAge: "1y",
        setHeaders(res, path) {
            if (path.includes("~/sw.js")) {
                res.setHeader("Cache-Control", `public, max-age=${15 * 60}`)
            }
        }
    }

},

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/ga.js', ssr: false },
    { src: '~/plugins/lazy-load-images.js', ssr: false },
    { src: '~/plugins/vue-social-share.js', ssr: false },
    // { src: '~/plugins/vue-svg-icon.js', ssr: false },
    { src: '~/plugins/swiper.js', ssr: false},
    { src: '~plugins/vue-select.js', ssr: false},
    { src: '~/plugins/vuex-persistedstate.js', ssr: false },
    { src: '~/plugins/vue-a.js', ssr: false },
  ],
  /*
  ** Nuxt.js dev-modules
  */
  devModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    //'@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'nuxt-svg-loader',
    '@nuxtjs/sitemap'
  ],

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: 'https://mauvegroup.com/',
    credentials : false,
    proxy: false,
    debug: false,
    // requestInterceptor: (config, {store}) => {
    //   config.headers.common['Access-Control-Allow-Origin'] = '*';
    //   config.headers.common['Content-Type'] = 'application/json';
    //   return config
    // }
  },
  
  serverMiddleware: [
    // API middleware
    '@/api/index.js',
  ],
  
   /*
  ** Build configuration
  */

  build: {
  /*
  ** You can extend webpack config here
  */


    optimization: {
        splitChunks: {
            name: true
        }
    },

    extend(config) { //ctx

      // config.node = {
      //     fs: 'empty'
      // }
      // SVG LOADER
      // const svgRule = config.module.rules.find(rule => rule.test.test('.svg'));

      // svgRule.test = /\.(png|jpe?g|gif|webp)$/;

      // config.module.rules.push({
      //     test: /\.svg$/,
      //     loader: 'vue-svg-loader',
      // });

      
    }
  }
}

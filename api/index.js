"use strict";

import express from "express"
import bodyParser from 'body-parser';
import axios from 'axios';

const cors = require("cors");

// Google reCaptcha secret
const GOOGLE_reCAPTCHA_SECRET = "6LcChLoUAAAAAOAIKdG7vZb7vFe9idt9G_iVqQOz";

// Gravity Forms API 
const GF_CONSUMER_KEY = 'ck_442bb779e831a9a39ed8bc8e2dbb3d1cdb27792c';
const GF_CONSUMER_SECRET = 'cs_e342ac39b33c428282096ed9d7e06dfec1cd30c2';

// Create base 64 token
let buff = Buffer.from(`${GF_CONSUMER_KEY}:${GF_CONSUMER_SECRET}`);
let base64data = buff.toString('base64');

let app = express();
app.use(cors());
app.use(bodyParser.json());

app.post('/contact', (req, res) => {

  const options = {
    headers: {'Authorization': `Basic ${base64data}`}
  };

  axios.post("https://www.mauvegroup.com/wp-json/gf/v2/forms/1/submissions", req.body, options).then( response => {

    res.json(response.data) 
    
  }).catch(function (error) {

    res.json(error) 

  })
 
  

});


app.post('/rest', (req, res) => {

  let endpoint = req.body.endpoint;

  axios.get(`https://www.mauvegroup.com/${endpoint}`).then( response => {

    res.json({
      headers: response.headers,
      data: response.data
    }) 
    
  }).catch(function (error) {

    res.json(error) 

  })

 
  

})

export default {
  path: '/api',
  handler: app
}

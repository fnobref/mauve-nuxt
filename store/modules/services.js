/** Store module to handle trips lists **/

import Vue from 'vue';
import axios from 'axios';

export default {

    namespaced: true,

    // ----------------------------------------------------------------------------------

    state: {

        services: [],
        loaded: false,
        selectedCountryService: null,

    },

    // ----------------------------------------------------------------------------------

    getters: {

        services: state => state.services,

        selectedCountryService: state => state.selectedCountryService,

        loaded: state => state.loaded

    },

    // ----------------------------------------------------------------------------------
    mutations: {

        setServices: (state, services) => {
            
            Vue.set(state, 'services', services)

        },
        
        setSelectedCountryService: (state, selectedCountryService) => {
            
            Vue.set(state, 'selectedCountryService', selectedCountryService)

        },

        loaded: state => Vue.set(state, 'loaded', true)

    },

    // ----------------------------------------------------------------------------------

    actions: {


        //  async nuxtServerInit( { dispatch } , countryID) {

        //      await dispatch('getCountries');

        // },
        
        // Set the selcted service for the clicked country on the home page country selection
        // We use the state (selectedCountryService) on the service page
        setSelectedCountryService(context, payload){
            context.commit('setSelectedCountryService', payload);
        },

        getServices(context, params) {

            return new Promise((resolve, reject) => {
                
                let services_endpoint = `wp-json/wp/v2/pages?parent=${params.servicesParentID}&lang=${params.lang}&order=asc`;

                axios.post('/api/rest', {endpoint:services_endpoint})
            
                .then(response => {

                    context.commit('setServices', response.data.data);
    
                    context.commit('loaded');  
                    
                    return resolve(context.state.services);
    
                })
    
                .catch(error => {
    
                    console.log(error)
                    // in case of error, empties the countries
                    context.commit('setServices', [])
    
                    return reject(error)
    
                })
            })
            
           

        }

    }
}
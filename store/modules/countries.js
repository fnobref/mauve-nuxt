/** Store module to handle trips lists **/

import Vue from 'vue';
import axios from 'axios';

export default {

    namespaced: true,

    // ----------------------------------------------------------------------------------

    state: {

        countries: {},
        loaded: false

    },

    // ----------------------------------------------------------------------------------

    getters: {

        countries: state => state.countries,

        loaded: state => state.loaded

    },

    // ----------------------------------------------------------------------------------
    mutations: {

        setCountries: (state, payload) => {

            state.countries[payload[0]] = payload[1]

        },

        loaded: state => Vue.set(state, 'loaded', true)

    },

    // ----------------------------------------------------------------------------------

    actions: {


        //  async nuxtServerInit( { dispatch } , countryID) {

        //      await dispatch('getCountries');

        // },


        getCountries(context, params) {

            return new Promise((resolve, reject) => {
                
                let countries_endpoint = `wp-json/wp/v2/country?parent=${params.continentID}&per_page=100&lang=${params.lang}`;

                axios.post('/api/rest', {endpoint:countries_endpoint})
                        
                .then(response => {
            
                    context.commit('setCountries', [params.continentID, response.data.data ]);
    
                    context.commit('loaded');  
                    
                    return resolve(context.state.countries);
    
                })
    
                .catch(error => {
    
                    console.log(error)
                    // in case of error, empties the countries
                    context.commit('setCountries', [])
    
                    return reject(error)
    
                })
            })
            
           

        }

    }
}
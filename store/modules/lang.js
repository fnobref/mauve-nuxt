
import Vue from 'vue';
import axios from 'axios';

export default {

    namespaced: true,

    // ----------------------------------------------------------------------------------

    state: {

        lang: "",
        translations: null,
        loaded: false,

    },

    // ----------------------------------------------------------------------------------

    getters: {

        lang: state => state.lang,

        translations: state => state.translations ? state.translations : "",

        loaded: state => state.loaded,

    },

    // ----------------------------------------------------------------------------------
    mutations: {

        setLang: (state, lang) => {

            Vue.set(state, 'lang', lang)
        },

        setTranslations: (state, translations) => {

            Vue.set(state, 'translations', translations)

        },

        loaded: state => Vue.set(state, 'loaded', true)

    },

    // ----------------------------------------------------------------------------------

    actions: {

        setLang(context, routeLang) {
              
            let lang = routeLang ? routeLang : "en";

            context.commit('setLang', lang);

        },

        getTranslations(context, lang){

    
            return new Promise( (resolve, reject) => {
                
                let url = `wp-json/acf/v2/options/?lang=${context.state.lang}`;
                
                
                axios.post('/api/rest', {endpoint:url})
            
                .then(response => {
     
                    context.commit('setTranslations', response.data.data.acf);
    
                    context.commit('loaded');  

                    return resolve(context.state.countries);
    
                })
    
                .catch(error => {
                    
                    console.log(error)
                    // in case of error, empties the countries
                    context.commit('setTranslations', [])
    
                    return reject(error)
    
                })
            })

        }
    }

}
import Vue from 'vue'
import Vuex from 'vuex'
import services from './modules/services';
import countries from './modules/countries';
import lang from './modules/lang';

Vue.use(Vuex)

const createStore = () => {

    return new Vuex.Store({

        modules: { services, countries, lang }

    });


};

export default createStore
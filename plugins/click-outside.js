import Vue from 'vue';

Vue.directive('click-outside', {

  bind: function (el, binding, vnode) {
    el.event = function (event) {
      if (!(el.contains(event.target))) {
          vnode.context.$emit('emitedEvent')
          console.log('eeeee')
      }else{
        console.log('ffffff')
      }
    }
    document.addEventListener('click', el.event)
  },


  unbind: function (el) {
    document.removeEventListener('click', el.event)
  },
})